var mps = {
    "unit": "m/s",
    "factor": 1
};

var kmph = {
    "unit": "km/h",
    "factor": 3.6
};

var mph = {
    "unit": "mph",
    "factor": 2.237
}

var units = [mps, kmph, mph];
