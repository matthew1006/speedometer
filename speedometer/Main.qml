import QtQuick 2.4
import Ubuntu.Components 1.3
import QtPositioning 5.2
import QtQuick.Window 2.2
import QtSystemInfo 5.0

import "speed-units.js" as Units

/*!
    \brief MainView with a Label and Button elements.
*/
Window{

    id: window
    x:0
    y:0
    width: units.gu(100)
    height: units.gu(75)

    visibility: Window.FullScreen
    visible: true
    //Avoid screen from going blank after some time...
    ScreenSaver {
        id: screenSaver
        screenSaverEnabled: !Qt.application.active
    }

    MainView {
        // objectName for functional testing purposes (autopilot-qt5)
        objectName: "mainView"

        // Note! applicationName needs to match the "name" field of the click manifest
        applicationName: "holospeedometer.matt1006"

        /*
         This property enables the application to change orientation
         when the device is rotated. The default is false.
        */
        //automaticOrientation: true

        anchors.fill: parent

        backgroundColor: "#000000"

        Page {

            header: PageHeader {
                exposed : false
            }

            Rectangle {
                id: speedometer

                anchors.fill: parent
                color: "transparent"

                property int unitIndex: 0

                transform: Scale {
                    id: scale
                    origin.y: window.height * 0.5
                    yScale: -1;

                    NumberAnimation on yScale {
                        id: flip
                        to: -scale.yScale
                        duration: 100
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        flip.start();
                    }

                }

                Label {
                    id: speed
                    objectName: "speed"

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    fontSizeMode: Text.HorizontalFit
                    font.pointSize: 70.0
                    anchors.fill: parent
                    color: "#ffffff"
                    font.bold: true

                    text: "0"
                }

                Label {
                    id: unitLabel
                    objectName: "units"

                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignBottom
                    fontSizeMode: Text.HorizontalFit
                    font.pointSize: 30.0
                    color: "#ffffff"
                    x: parent.width - width
                    y: parent.height - height

                    text: "m/s"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            var newIndex = speedometer.unitIndex + 1;
                            if (newIndex >= Units.units.length) newIndex = 0;
                            unitLabel.text = Units.units[newIndex].unit;
                            speed.text = (parseFloat(speed.text) * Units.units[newIndex].factor / Units.units[speedometer.unitIndex].factor).toFixed(0);
                            speedometer.unitIndex = newIndex;
                        }

                    }
                }
            }

            PositionSource {
                id: src
                updateInterval: 1000
                active: true

                property point lastCoord : "0,0"
                property real cEQ : 4.0075017e7     //Circumference of earth at equator
                property real cPL : 3.9940652742e7  //Circumference of earth pole to pole

                //Calculate your change in latitude and longitude in meters and calculate the total distance traveled using
                //Pythagoras' theorem by approximating the surface of the earth as being flat over short distances.
                //Note: This breaks when crossing the international date line but that's all ocean so who cares?!
                function approximateSpeed (coord) {

                    var dX = src.cEQ * (coord.longitude - src.lastCoord.x) * Math.cos (coord.latitude * Math.PI / 360) / 360;
                    var dY = src.cPL * (coord.latitude - src.lastCoord.y) / 360;

                    var s = Math.sqrt (dX*dX + dY*dY);
                    return isNaN (s) ? 0 : s;
                }

                onPositionChanged: {
                    var coord = src.position;

                    var s = 0;
                    if (isNaN(coord.speed))
                    {
                        s = src.approximateSpeed (coord.coordinate);
                    }
                    else
                    {
                        s = coord.speed;
                    }

                    speed.text = (s * Units.units[speedometer.unitIndex].factor).toFixed(0)

                    src.lastCoord.x = coord.coordinate.longitude;
                    src.lastCoord.y = coord.coordinate.latitude;
                }
            }
        }
    }
}

